# README #

### NOTICE OF GREAT IMPORTANCE ###
* PLEASE READ THE ENTIRE NOTICE BEFORE ATTEMPTING TO USE THIS MODULE ON TEST OR LIVE SERVERS:
* Drupal's version of cron runs as the www-data user and cannot execute git commands as a result. In order for this module to work properly outside of a sandbox, you must give it permission to execute git commands as the user which owns the website repository. This is done in the sudoers file.
* IF YOU NEED TO EDIT THE SUDOERS FILE, USE THE VISUDO COMMAND!! YOU CAN ALSO APPEND TO THE SUDOERS FILE WITH THE SUDOERS.D DIRECTORY BUT YOU SHOULD ALWAYS USE VISUDO EVEN WHEN EDITING FILES IN THAT DIRECTORY. USING VISUDO FORCES SYNTAX TO BE CHECKED AND PREVENTS YOU FROM ACCIDENTLY BREAKING AN ENTIRE LINUX INSTALL. IF YOU MESS UP THE SYNTAX AND SAVE THE FILE YOU CANNOT EXECUTE ANYTHING WITH ROOT PERMISSIONS AND YOU CAN NO LONGER EDIT THE SUDOERS FILE TO UNDO YOUR MISTAKE. USE VISUDO!!!
https://www.sudo.ws/docs/man/1.8.13/visudo.man/
### ONCE AGAIN: ###
* IF YOU NEED TO EDIT THE SUDOERS FILE, USE THE VISUDO COMMAND!! YOU CAN ALSO APPEND TO THE SUDOERS FILE WITH THE SUDOERS.D DIRECTORY BUT YOU SHOULD ALWAYS USE VISUDO EVEN WHEN EDITING FILES IN THAT DIRECTORY. USING VISUDO FORCES SYNTAX TO BE CHECKED AND PREVENTS YOU FROM ACCIDENTLY BREAKING AN ENTIRE LINUX INSTALL. IF YOU MESS UP THE SYNTAX AND SAVE THE FILE YOU CANNOT EXECUTE ANYTHING ROOT PERMISSIONS AND YOU CAN NO LONGER EDIT THE SUDOERS FILE TO UNDO YOUR MISTAKE. USE VISUDO!!!
https://www.sudo.ws/docs/man/1.8.13/visudo.man/

## visudo config
Run `sudo visudo` and add this to the bottom of the file:
```
## Allow web user to run git commands, see cyberwoven/telemetry module
www-data ALL=(cyberwoven) NOPASSWD: /usr/bin/git
```
# Install

1. Add the module's git repo to the site's composer.json

```json
{
  "type": "vcs",
  "url": "git@bitbucket.org:cyberwoven/telemetry.git"
}
```

2. Run this command:

```bash
composer require cyberwoven/telemetry
# It will be in modules/contrib/
```

3. Remove the .git directory from the module:

```bash
cd pub/modules/contrib/telemetry
rm -rf .git
```

# Drush commands

1. For the moment, this just displays a dummy json object on the command line
 * It will only display a notice and log to recent reports if logging is enabled in the module settings
```bash
drush telemetry:show
```

2. For the moment, this mereley says that it sends data of some kind, it really only prints to the command line
 * It will only display a notice and log to recent reports if logging is enabled in the module settings
```bash
drush telemetry:send
```

Currently this module:

* Logs 'Hello World with a bit of dummy functionality!' to recent log messages whenever cron runs if the enable telemetry option is checked in the admin form for the module

* Exposes persistent settings for the module to a user with proper permissions through a form in the Drupal admin (/admin/telemetry)

* Collects and logs the name of the project the module is installed on (i.e. www.example.com) as well as information about the current commit of that project in form of a json object