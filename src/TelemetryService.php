<?php

namespace Drupal\telemetry;

use Drupal\Core\Database\Database;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Extension\ExtensionList;
use Drupal\Core\Extension\Extension;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;

/**
 * Class TelemetryService.
 * 
 * @package Drupal\telemetry
 */
class TelemetryService {

  private $invoking_username = '';
  private $git_username = '';
  
  public function __construct() {
    if (function_exists('posix_geteuid') && function_exists('posix_getpwuid')) {
      $this->invoking_username = posix_getpwuid(posix_geteuid())['name'];
      $this->git_username = posix_getpwuid(fileowner(DRUPAL_ROOT))['name'];
    }
  }

  /**
   * Returns a json object containing the project name
   * based on the composer.json name property
   * and information about the current commit
   */
  public function collect() {
    $config = \Drupal::config('telemetry.settings');
    $logging_enabled = $config->get('logging_enabled');

    try {
      $branch_name = $this->getShellOutput('git rev-parse --abbrev-ref HEAD');
      $commit_hash = $this->getShellOutput('git rev-parse --short HEAD');
      $commit_date = $this->getShellOutput('git log -1 --format=%cd --date=local');
      $tags =        $this->getShellOutput('git tag --points-at HEAD');
    } catch (\Exception $e) {
      if ($logging_enabled) {
        \Drupal::logger('telemetry')->error("Unable to collect telemetry data: " . $e->getMessage());
        return;
      }
    }

    $project_name = getenv('PANTHEON_SITE_NAME');
    if (empty($project_name)) {
      $composer_file = file_get_contents(DRUPAL_ROOT . '/../composer.json', FILE_USE_INCLUDE_PATH); //file_get_contents will start from the pub directory in the context of a module
      $composer_json = json_decode($composer_file, false);
      $project_name = basename($composer_json->name);
    }

    /**
     * The deployment_tier configuration can be set in php.ini
     * 
     * For example:
     *  deployment_tier=test
     * 
     * If it's not set, we check for Pantheon hosting and use their environment.
     * 
     * If that's not set, we assume it's a live site.
     */  
    $tier = get_cfg_var('deployment_tier') ?: getenv('PANTHEON_ENVIRONMENT') ?: 'live';
    
    $data = [];
    $data["project"] = $project_name;
    $data["tier"] = strtolower($tier);
    $data["host"] = \Drupal::request()->getHost();

    /**
     * If we have the branch name, assume we have all the other info from git, so
     * set that stuff in the payload.
     */
    if (!empty($branch_name)) {
      $data["commit"] = [
        "branch" => $branch_name,
        "hash" => $commit_hash,
        "date" => $commit_date,
      ];
      if (!empty($tags)) {
        $data["commit"]["tags"] = explode("\n", $tags);
      }
    }

    // Get the Drupal core version and PHP version.
    $core_version = \Drupal::VERSION;
    $php_version = phpversion();

    // Get the database connection.
    $database = Database::getConnection();

    // Get the database version for MySQL.
    if ($database->driver() == 'mysql') {
      $result = $database->query("SELECT VERSION()");
      $db_version = $result->fetchField();
    } else {
      $db_version = null;
    }

    // Save the version information to an array.
    $data['versions'] = [
      'core' => $core_version,
      'php' => $php_version,
      'database' => $db_version,
    ];

    // Get the module handler and extension list services.
    $module_handler = \Drupal::service('module_handler');
    $module_extension_list = \Drupal::service('extension.list.module');
    // var_dump($module_exten sion_list->getList());
    // Get a list of all the modules.
    $enabled_modules = $module_extension_list->getList();
    $disabled_modules = [];
    // Initialize an array to hold the module information.
    $module_info = [];

    // Iterate over the modules and add their information to the array.
    foreach ($enabled_modules as $module_name => $module) {
      // If the module is enabled or exists, get the module info, else handle differently
      // Get the module configuration.
      if($module_handler->moduleExists($module_name)) {
        $module_info = $module_extension_list->getExtensionInfo($module_name);
      } else {
        // Can't use getExtensionInfo on modules that are not enabled,
        // so you must access the module properties directly
        $module_data[] = [$module_name, $module_handler->getName($module_name), $module->info['version'], $module->status];
        continue;
      }

      // Get the module version.
      $module_version = isset($module_info['version']) ? $module_info['version'] : '';

      // Check if the module is enabled.
      $module_status = $module_info ? 1 : 0;

      // Add the module information to the array.
      $module_data[] = [$module_name, $module_handler->getName($module_name), $module_version, $module_status];
    }

    $data['modules'] = $module_data;

    $json_data = json_encode($data);

    if ($logging_enabled) {
      \Drupal::logger('telemetry')->notice($json_data);
      \Drupal::logger('telemetry')->notice('Collected Telemetry data');
    }

    return $json_data;
  }

  /**
   * Will eventually POST the collected data to configured the HTTP endpoint.
   */
  public function send() {
    // Get config
    $config = \Drupal::config('telemetry.settings');

    // Get config values
    $url = $config->get('url');
    $project_secret = $config->get('secret_key');
    $logging_enabled = $config->get('logging_enabled');

    // Collect payload data
    $payload = $this->collect();

    // Create request options
    $options = [
      'headers' => [
        'Content-Type' => 'application/json',
        'Authorization' => 'Bearer ' . $project_secret
      ],
      'body' => $payload
    ];

    // Send POST request
    $response = \Drupal::httpClient()->post($url, $options);
    $body = $response->getBody();

    // Logging
    if ($logging_enabled) {
      \Drupal::logger('telemetry')->notice("Data collected:\n" . $payload);
      \Drupal::logger('telemetry')->notice("Response body:\n" . $body);
    }

    // Decode
    $data = json_decode($body);

    return $data;
    
  }

  /**
   * Run a command as the appropriate user and return the output
   */
  private function getShellOutput($cmd) {
    if ($this->invoking_username !== $this->git_username) {
      $cmd = sprintf("sudo -u %s %s", $this->git_username, $cmd);
    }

    $proc = Process::fromShellCommandline($cmd);
    $proc->run();

    if (!$proc->isSuccessful()) {
      throw new ProcessFailedException($proc);
    }

    return trim($proc->getOutput());
  }
}