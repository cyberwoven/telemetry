<?php

/**
 * @file
 * Contains \Drupal\telemetry\Form\TelemetryModuleSettingsForm
 */
namespace Drupal\telemetry\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Cache\Cache;

/**
 * Configure telemetry settings for this site.
 */
class TelemetryModuleSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'telemetry_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'telemetry.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('telemetry.settings');

    $form['telemetry_cron_enabled'] =[
      '#type' => 'checkbox',
      '#title' => $this->t('Enable telemetry cron hook.'),
      '#default_value' => $config->get('cron_enabled'),
    ];

    $form['telemetry_logging_enabled'] =[
      '#type' => 'checkbox',
      '#title' => $this->t('Enable logging.'),
      '#default_value' => $config->get('logging_enabled'),
    ];

    $form['telemetry_url'] = [
      '#type' => 'url',
      '#title' => $this->t('API Endpoint'),
      '#placeholder' => $this->t('https://api.example.com/'),
      '#description' => $this->t('The HTTP endpoint that telemetry data will be sent to'),
      '#default_value' => $config->get('url')
    ];

    $form['telemetry_secret_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Secret Key'),
      '#placeholder' => $this->t('Secret Key'),
      '#description' => $this->t('Project-specific secret key, sent as a bearer token in the Authorization header.'),
      '#default_value' => $config->get('secret_key'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $config = \Drupal::service('config.factory')->getEditable('telemetry.settings');
    $config
      ->set('cron_enabled', $form_state->getValue('telemetry_cron_enabled'))
      ->set('logging_enabled', $form_state->getValue('telemetry_logging_enabled'))
      ->set('secret_key', $form_state->getValue('telemetry_secret_key'))
      ->set('url', $form_state->getValue('telemetry_url'))
      ->save();

    Cache::invalidateTags(array('config:telemetry.settings'));
  }
}
