<?php

namespace Drupal\telemetry\Commands;

use Drush\Commands\DrushCommands;
use Drupal;

/**
 * The drush command file for the telemetry file
 * 
 * @package Drupal\telemetry\Commands
 */
class CustomDrushCommands extends DrushCommands {

  /**
   * Drush command that displays the telemetry payloud on stdout.
   * 
   * @command telemetry:show
   * @option action
   * @usage telemetry:show
   */
  public function show() {
    $telemetry = Drupal::service('telemetry.telemetry_service');
    $this->output()->writeln($telemetry->collect());
  }

  /**
   * Drush command that fires the telemetry service's send() method.
   * 
   * @command telemetry:send
   * @option action
   * @usage telemetry:send
   */
  public function send() {
    $config = \Drupal::config('telemetry.settings');
    $logging_enabled = $config->get('logging_enabled');

    $telemetry = Drupal::service('telemetry.telemetry_service');
    if($logging_enabled) {
      $this->output()->writeln('Sending data to HTTP endpoint...');
    }
    $telemetry->send();
  }

  /**
   * Drush command that displays the $_SERVER autoglobal array.
   * 
   * @command telemetry:server
   * @option action
   * @usage telemetry:server
   */
  public function server() {
    $this->output()->writeln(json_encode($_SERVER));
  }
}
